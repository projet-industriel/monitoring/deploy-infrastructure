output "bastion_floatip" {
  value = module.bastion.floatip
  description = "IP flottante de l'instance bastion"

  depends_on = [
    module.bastion
  ]
}

output "edge_floatip" {
  value = module.edge.floatip
  description = "IP flottante du manager node"

  depends_on = [
    module.edge
  ]
}

output "nodes_internal_ip" {
  value = { for node in concat([module.bastion], [module.edge], module.manager, module.worker): node.name => node.access_ip_v4 }
  description = "IP interne des instances"

  depends_on = [
    module.bastion,
    module.edge,
    module.manager,
    module.worker
  ]
}
