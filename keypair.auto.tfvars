# Nom de la paire de clé sur Openstack
keypair_name     = "r22fetch_kp"
# Chemin sur le poste local de la clé privée ssh de connexion aux instances
PRIVATE_KEY_PATH = "keys/r22fetch_kp.pem"
