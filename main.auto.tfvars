external_network      = "external"     # Nom du réseau externe

int_network = {
  name = "int_network"
  subnet = "int_subnet"
  cidr = "172.16.102.0/24"
  dns = ["192.44.75.10", "192.108.115.2"]
  router = "int_router"
}

int_secgroup = {
  name     = "sg_node"
  description = "Nom du groupe de sécurité du réseau interne"
}

int_secgroup_rules    = [
  { "source" = "172.16.102.0/24", "protocol" = "tcp", "port" = 0 },
  { "source" = "172.16.102.0/24", "protocol" = "udp", "port" = 0 },
]

bastion_secgroup = {
  name     = "sg_bastion"
  description = "Groupe de sécurité de l'instance bastion"
}

bastion_secgroup_rules    = [
  { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 22 },
  { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 6443 }
]

app_secgroup = {
  name     = "sg_manager"
  description = "Nom du groupe de sécurité du noeud manager qui sert l'app"
}

app_secgroup_rules    = [
  { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 80 },
  { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 443 },
  { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 30000 }
]

bastion               = {
  flavor              = "s20.large"     # 2CPU, 2Go RAM, 10Go
  name                = "bastion"
  name_suffix         = "rke"
  image               = "ubuntu-22.04"   # Nom de l'image OS de bastion
  user                = "ubuntu"
  ssh_port            = 22
  role                = ["gateway"]
}

manager = {
  flavor           = "s20.large"    # 2CPU, 4Go RAM, 20 Go
  name_prefix      = "control"
  name_suffix      = "rke"
  image            = "ubuntu-22.04"     # Nom de l'image OS des noeuds
}

node = {
  flavor           = "s20.xlarge"       # 4CPU, 8Go RAM, 20Go
  name_prefix      = "worker"
  name_suffix      = "rke"
  image            = "ubuntu-22.04"     # Nom de l'image OS des noeuds
  user             = "ubuntu"
  ssh_port         = 22
}

manager_role = [ "control" ]
worker_role = [ "worker" ]
manager_count         = 3
worker_count          = 3
manual_trigger        = 0
