variable "keypair_name" {
  type = string
  description = "Nom de la paire de clés SSH"
  default = "keypair_tf"
  validation {
    condition = length(var.keypair_name) > 2
    error_message = "Le nom de la paire de clés SSH doit contenir plus de 2 caractères"
  }
}

variable "PRIVATE_KEY_PATH" {
  type = string
  description = "Chemin de la clé privée de connexion ssh au bastion"
  default = "keys/demo.pem"
}

variable "external_network" {
  type = string
  description = "Nom du réseau externe"
  default = "external"
}

variable "int_network" {
  type = object({
    name = string
    subnet = string
    cidr = string
    dns = list(string)
    router = string
  })

  default = {
    cidr = "172.16.102.0/24"
    dns = ["94.16.114.254", "94.247.43.254"] # DNS OpenNIC
    name = "internal_network"
    router = "internal_router"
    subnet = "internal_subnet"
  }

  validation {
    condition = can(cidrhost(var.int_network.cidr, 0))
    error_message = "Le cird du sous-réseau est invalide"
  }

  validation {
    condition = alltrue([
        for dns in var.int_network.dns : can(cidrhost("${dns}/32", 0))
    ])
    error_message = "Les adresses IP des DNS sont invalides"

  }

}

variable "int_secgroup" {
  type = object({
    name = string
    description = string
  })
  default = {
    name = "int_secgroup"
    description = "Groupe de sécurité du réseau interne"
  }
}

variable "int_secgroup_rules" {
  type = list(object({
    source = string
    protocol = string
    port = number
  }))
  description = "Règles du groupe de sécurité u réseau interne"
  default = [ {
    port = 0
    protocol = "tcp"
    source = "172.16.102.0/24"
  } ]
}

variable "bastion_secgroup" {
  type = object({
    name = string
    description = string
  })
  default = {
    name = "bastion_secgroup"
    description = "Groupe de sécurité de l'instance bastion"
  }
}

variable "bastion_secgroup_rules" {
  type = list(object({
    source = string
    protocol = string
    port = number
  }))
  description = "Règles du groupe de sécurité de bastion"
  default = [ {
    port = 22
    protocol = "tcp"
    source = "172.16.102.0/24"
  } ]
}

variable "app_secgroup" {
  type = object({
    name = string
    description = string
  })
  default = {
    name = "app_secgroup"
    description = "Groupe de sécurité du noeud manager qui sert l'app"
  }
}

variable "app_secgroup_rules" {
  type = list(object({
    source = string
    protocol = string
    port = number
  }))
  description = "Règles du groupe de sécurité des noeuds qui sert l'application"
  default = [ {
    port = 80
    protocol = "tcp"
    source = "172.16.102.0/24"
  } ]
}

variable "bastion" {
  type = object({
    name = string
    name_suffix = string
    flavor = string
    image = string
    user = string
    ssh_port = number
    role = list(string)
  })

  default = {
    flavor = "m1.medium"  # m1-medium : 2 cpu, 2Go ram, 5Go disque
    image = "ubuntu-22.04"
    name = "bastion_tf"
    name_suffix = "rke.kube"
    role = [ "bastion" ]
    ssh_port = 22
    user = "ubuntu"
  }
}

variable "node" {
  type = object({
    name_prefix = string
    name_suffix = string
    flavor = string
    image = string
    user = string
    ssh_port = number
  })

  default = {
    name_prefix = "worker"
    name_suffix = "rke.kube"
    flavor = "m1.medium"  # m1-medium : 2 cpu, 2Go ram, 5Go disque
    image = "ubuntu-22.04"
    ssh_port = 22
    user = "ubuntu"
  }
}

variable "manager" {
  type = object({
    name_prefix = string
    name_suffix = string
    flavor = string
    image = string
  })

  default = {
    name_prefix = "control"
    name_suffix = "rke.kube"
    flavor = "s20.xlarge.4g"  # 4CPU, 4Go RAM, 20Go
    image = "ubuntu-22.04"
  }
}

variable "manager_role" {
  type = list(string)
  description = "Tags : rôle du manager"
  default = ["manager"]
}

variable "worker_role" {
  type = list(string)
  description = "Tags : rôle du worker"
  default = ["worker"]
}

variable "manager_count" {
  type = number
  description = "nombre de noeuds managers"
  default = 1
}

variable "worker_count" {
  type = number
  description = "nombre de noeuds workers"
  default = 0

}

variable "manual_trigger" {
  type = number
  description = "Variable pour lancer la null ressource qui génère le l'inventaire"
  default = 0
}
