variable "secgroup" {
  type = object({
    name = string
    description = string
  })
  default ={
      name = "sg_internal"
      description = "Groupe de sécurité du réseau interne"
    }
}


variable "secgroup_rules" {
  type = list(object({
    source = string
    protocol = string
    port = number
  }))
  default = [
    {
      source = "172.16.102.0/24"
      protocol = "tcp"
      port = 0
    }
  ]
}
