resource "null_resource" "inventory" {

  triggers = {
    instance_name    = join(",", var.instances.*.name),
    instance_ip      = join(",", var.instances.*.access_ip_v4)
    instance_floatip = join(",", compact(var.instances.*.floatip))
    instance_role    = join(",", flatten(var.instances.*.role))
    manual_run       = var.manual_trigger
    #always_run = timestamp()
  }

  connection {
    host        = var.ssh_bastion.floatip
    type        = "ssh"
    port        = var.ssh_bastion.port
    user        = var.ssh_bastion.user
    private_key = file(var.ssh_bastion.keypair_path)
    agent       = "false"
  }

  provisioner "local-exec" {
    command = <<-EOT
            mkdir -p inventory
            rm -r inventory/*
            %{ for role in distinct(flatten(var.instances.*.role)) ~}
                echo "[${role}]" > inventory/${role}_ansible
                echo "[${role}]" > inventory/${role}

                %{ for instance in var.instances ~}
                    %{ if contains("${instance.role}", "${role}") ~}
                      echo "${instance.name}" >> inventory/${role}_ansible
                      echo "${instance.name}  ${instance.access_ip_v4}" >> inventory/${role}
                    %{ endif ~}
                %{ endfor ~}

                cat inventory/${role}_ansible | tee -a inventory/hosts.ini
                cat inventory/${role} | tee -a inventory/private_ip.ini
                rm -rf inventory/${role}_ansible inventory/${role}
            %{ endfor ~}

            %{ for instance in var.instances ~}
                %{ if instance.floatip != null ~}
                  echo '[${join("-", "${instance.role}")}]' >> inventory/public_ip.ini
                  echo "${instance.name} : ${instance.floatip}" >> inventory/public_ip.ini
                %{ endif ~}
            %{ endfor ~}
        EOT
  }
}

# setsubtract(distinct(flatten(var.instances.*.role)), var.bastion_role)
# var.instances.*.role est une liste de listes
# flatten permet de l'applatir
# distint retire les doublons
# setsubtract retire le role du bastion pour qu'il n'apparaisse pas dans l'inventaire
