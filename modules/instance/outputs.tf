output "name" {
  value = openstack_compute_instance_v2.vm.name
}

output "floatip" {
  value = var.floatip ? openstack_networking_floatingip_v2.fip[0].address : null
}

output "access_ip_v4" {
  value = openstack_compute_instance_v2.vm.access_ip_v4
}

output "role" {
  value = openstack_compute_instance_v2.vm.tags
}