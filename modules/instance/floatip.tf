resource "openstack_networking_floatingip_v2" "fip" {
  count = var.floatip ? 1 : 0
  pool        = var.external_network
  description = "IP flottante ${var.name}"
}

resource "openstack_compute_floatingip_associate_v2" "fip" {
  count = var.floatip ? 1 : 0
  floating_ip = openstack_networking_floatingip_v2.fip[0].address
  instance_id = openstack_compute_instance_v2.vm.id

  depends_on = [
    openstack_networking_floatingip_v2.fip,
    openstack_compute_instance_v2.vm
  ]
}