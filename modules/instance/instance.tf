# Instance bastion
resource "openstack_compute_instance_v2" "vm" {
  name            = var.name
  image_id        = data.openstack_images_image_v2.image.id
  flavor_id       = data.openstack_compute_flavor_v2.flavor.flavor_id
  key_pair        = var.keypair_name
  security_groups = var.secgroups

  network {
    name = var.network_name
  }

  tags = var.tags_role
}
